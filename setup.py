import setuptools

with open("README.md", "r") as fh:
    long_description = fh.read()

setuptools.setup(
    name="sqlsettingslib",
    version="0.0.4",
    author="Fabian Becker",
    author_email="fab.becker@outlook.de",
    description="Store settings in a sql database",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://gitlab.com/Raspilot/sqlsettingslib",
    packages=setuptools.find_packages(),
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: GNU General Public License v3 (GPLv3)",
        "Operating System :: OS Independent",
    ],
    python_requires='>=3.6',
)