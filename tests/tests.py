import sqlsettingslib
import os
from quicktests import HRTest


class Test(HRTest):
    def test_setting_returns_stored_values(self):
        s = sqlsettingslib.Setting("test_db.sqlite3")
        s.add_setting(text_id="user_name",
                      readable_name="User Name",
                      description="A unique user name",
                      value="test-user")
        assert s["user_name"] == "test-user", "Values are not being restored correctly."


if __name__ == '__main__':
    t = Test()
    result = t()
    try:
        os.remove("test_db.sqlite3")
    except FileNotFoundError:
        pass
    exit(result)
